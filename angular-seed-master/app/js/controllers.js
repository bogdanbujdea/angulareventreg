'use strict';

/* Controllers */

angular.module('myApp.controllers', []).
  controller('MyCtrl1', [function() {

  }])
  .controller('MyCtrl2', [function() {

  }])
  .controller('HelloCtrl', function($scope, $timeout){

        $timeout(function(){
            $scope.helloMessage = "Hello World"
        }, 3000);
    });